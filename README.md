# COMP 620 Analysis of Algorithms

## Purpose of this repo:
<div>
    <p>This is intended to act as a collection of notes for topics covered in comp 620. Each week is a directory
    consisting of a .md file and can have other code or plain text examples.</p>
</div>

## Code Of Conduct
<div>
    <p>Any commits with sensitive information to HW or test answers will result in instant removal from the repo.</p>
</div>

## Want to contribute?
clone the project:
<pre>
<code>
git pull https://gitlab.com/hasanaburayyan/comp-620-notes.git
</code>
</pre>
Make some changes, commit them and push to a new branch
<pre>
<code>
git commit -m "Some Comment"
git push origin HEAD:Branch_Name
</code>
</pre>
Then visit the link in the response
<pre>
<code>
remote: To create a merge request for Example_MR, visit:
remote:   https://gitlab.com/hasanaburayyan/comp-620-notes/-/merge_requests/new?merge_request%5Bsource_branch%5D=Example_MR
</code>
</pre>
Click on submit merge request, rule of thumb right now is just have one fellow classmate thumb the changes and your are good to merge.