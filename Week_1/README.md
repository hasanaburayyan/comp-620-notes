# Week One Notes

## What is an Algorithm?
<ul>
    <li>A method that can be used by a computer for the solution of a problem</li>
    <li>A finite set of instructions that if followed, accomplishes a particular task</li>
    <li>ALL Algorithms must satisfy the following criteria:
        <ol>
            <li>Input: Zero or more quantities are externally supplied</li>
            <li>Output: At least one quantity is produced</li>
            <li>Definiteness: Each instruction is clear and unambiguous</li>
            <li>Finiteness: if we trace out the instructions of an algorithm then for all cases
                the algorithm terminates after a finite number of steps</li>
            <li>Effectiveness: Every instruction must be very basic, so that it can be carried
                out, in principle by a person using only pencil and paper</li>
        </ol>
    </li>
</ul>

## Dynamic Programing
<div>
    <p>
        Dynamic programming is useful for solving very difficult problem and will be a highlight of this course. In order to understand
       dynamic programming we will first need to cover:
       <ul>
        <li>
        How to devise algorithms
            <ul>
                <li><i>Creating an algorithm is an art that may never be fully automated</i></li>
            </ul>
        </li>
        <li>
        How to validate algorithms
            <ul>
                <li><i>Once an algorithm has been devised it is necessary to show that it computes the correct answer for all possible legal inputs</i></li>
            </ul>
        </li>
        <li>
        How to analyze programs
            <ul>
                <li><i>This field of study is called analysis of algorithms which refers to a task of determining how much computing time and storage an algorithm requires</i></li>
            </ul>
        </li>
        <li>
        How to test a program <b>(This is not covered in this course)</b>
            <ul>
                <li><i>Testing a program consists of two phases which are known as debugging and profiling (performance measurement)</i></li>
            </ul>
        </li>
       </ul>
    </p>
</div>

### Example Disclaimers
<div>
    <p>
        The following are important "GOTCHAS" to know about the examples in this course!
    </p>
</div>
<ul>
    <li>Array indices start at index of 1 <b>NOT</b> 0</li>
</ul>

## Insertion Sort Example
<div>
    <p>
        <b>Insertion-Sort (A) Cost Times (Iterations)</b><br>
        <i>The following example is psuedocode which is language agnostic meaning it is not written in or bound to a specific language (java, C, python, etc)</i>
<pre>
    <code>
    for j = 2 to length(A)                  c1      n
    key = A[j]                              c2      n - 1
    i = j -1                                c3      n - 1
    while i > 0 and A[i] > key              c4      
        A[i+1] = A[i]                       c5
        i = i -1                            c6
    A[i+1] = key                            c7
    </code>
</pre>
    <ul>
        <li>
            c1: We have j as an index pointer which goes from two to the last element (length of A).
        </li>
        <li>
            c2: Each iteration of the for loop, key is assigned the value j of A OR A[j].
            <ul>
                <li>So for the first iteration of this loop, j = 2 which means that the <i>key</i> is assigned the second element in the array <i>A</i></li>
            </ul>
        </li>
        <li>
            c3: For comparison we need a second index, we will call this index i. Which will be assigned a value of <i>i - 1</i>.
            <ul>
                <li>For the first iteration of this loop, j = 2 so that means that the index <i>i</i> will be assigned the value of 1</li>
            </ul>
        </li>
        <li>
            c4: This line marks the start of our while loop, we check that the value of i is greater than 0 AND that A[i] > key
            <ul>
                <li>The reason we check that i > 0 is because in insertion sort we swap numbers towards the begging of the array, however the arrays cannot have an index of 0 so if i = 0 then that means we are at the start of the array</li>
                <li>The second check is to see if the element in A[i] is greater than <i>key</i></li>
                <li><b>If both of these conditions are true than we need to perform a <i>SHIFT</i></b></li>
            </ul> 
        </li>
        <li>
            c5: For the <i>SHIFT</i> we need to set the element in A[i+1] to equal the element before itself
        </li>
        <li>
            c6: We then need to decrement the value of i by 1
        </li>
        <li>
            c7: Once we have exited our while loop we place <i>Key</i> into the index of i + 1
            <ul>
                <li>
                    For example if our key was the smallest number in the array then the loop would keep looping until i = 0. Then the key gets placed in index i + 1 (or 1)
                </li>
            </ul>
        </li>
    </ul>
    </p>
    <p>
<pre>
    <code>
     A = [5 , 2, 4, 6, 1, 3]
     c1: start for loop (j=2) length(A)=6
     c2: key = A[2] (which is 2)
     c3: i = j -1 (which is 1)
     c4: while i > 0 and A[i] > key (which evals on iteration one to: 1 > 0 (true) and 5 > 2 (true))
     c5: A[i+1] = A[i] (Shifts our array to [5, 5, 4, 6, 1, 3])
     c6: i = i - 1 (i = 1 - 1) This breaks our while loop since i = 0
     c7: now we set A[i+1] to key (this changes our array to [2, 5, 4, 6, 1, 3]) 
     Rinse and repeat
    </code>
</pre>
    </p>
</div>


